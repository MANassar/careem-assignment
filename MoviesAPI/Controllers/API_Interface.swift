//
//  API_Interface.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 07/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import Foundation
import Alamofire

struct Parameters
{
    static let page = "page"
    static let api_key = "api_key"
    static let query = "query"
}

enum ContentType
{
    case json
    case xml
}

enum Errors:Error
{
    case parsingError
    case urlError
    case responseFailure
}

enum ImageWidth:String
{
    case Width_92 = "92"
    case Width_185 = "185"
    case Width_500 = "500"
}

class API_Interface
{
    //
    // MARK:- Constants
    //
    private static let baseMoviesURLString = "http://api.themoviedb.org/3/search/movie"
    private static let apiKey = "2696829a81b1b5827d515ff121700838"
    
    private static let imagesBaseURLString = "http://image.tmdb.org/t/p/w"
    
    private static let defaultParameters:[String:String] = [ //Used with each request
        Parameters.api_key : apiKey,
    ]
    
    //
    // MARK:- Public Functions
    //
    
    /*
     This method retrieves the movies for a specific query if it could retrieve any results
     The page parameter is optional, if it is not supplied, the default value of 1 will be used to get the first page
     */
    class func getMovies(searchQuery:String, page:Int = 1, completion: @escaping ((MovieResponse?, Error?) -> Void))
    {
        self.getMoviesFromJSON(searchString: searchQuery, page: page, completion: completion)
    }
    
    class func getImage(urlPostfix:String, width:ImageWidth, completion:@escaping ((UIImage?, Error?) -> Void))
    {
        if let imagesURL = self.getImageURL(urlPostfix, width)
        {
            Alamofire.request(imagesURL).validate().responseData { (response) in
                if let error = response.error
                {
                    completion(nil, error)
                }
                else //We have no errors, try parsing
                {
                    response.result.ifSuccess
                        {
                            if let imageData = response.data, let image = UIImage(data: imageData)
                            {
                                completion(image, nil)
                            }
                            else
                            {
                                completion(nil, Errors.parsingError)
                            }
                    }
                    
                    response.result.ifFailure
                        {
                            completion(nil, Errors.responseFailure)
                    }
                }
            }
        }
        else
        {
            completion(nil, Errors.urlError)
        }
        
    }
    
    //
    // MARK:- Private/helper Functions
    //
    
    class func getImageURL(_ urlPostfix:String, _ width:ImageWidth) -> URL?
    {
        let fullURLString = imagesBaseURLString + width.rawValue + urlPostfix //final URL should look like http://image.tmdb.org/t/p/w<width>/<postfix>
        let imagesURL = URL(string: fullURLString)
        return imagesURL
    }
    
    private class func getMoviesFromJSON(searchString:String, page:Int = 1, completion: @escaping ((MovieResponse?, Error?) -> Void))
    {
        if let dataRequest = self.prepareMoviesDataRequest(searchString, page)
        {
            dataRequest.responseJSON { (response) in
                //Do we have any errors?
                if let error = response.error
                {
                    completion(nil, error)
                }
                else //All well, handle the response
                {
                    response.result.ifSuccess
                    {
                        if let value = response.result.value, let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
                        {
                            if let movieResponse = ResponseDecoder.decodeResponseJSONData(jsonData)
                            {
                                completion(movieResponse, nil)
                                return
                            }
                            else
                            {
                                completion(nil, Errors.parsingError)
                            }
                        }
                    }
                    
                    response.result.ifFailure
                    {
                        completion(nil, Errors.responseFailure)
                        return
                    }
                }
            }
        }
        else
        {
            completion(nil, Errors.urlError)
        }
    }
    
    class func prepareMoviesDataRequest (_ searchString:String, _ page:Int = 1) -> DataRequest?
    {
        var parameters = defaultParameters
        parameters[Parameters.query] = searchString
        parameters[Parameters.page] = String(page)
        
        guard let movieSearchURL = URL(string: baseMoviesURLString) else {
            return nil
        }
        
        return Alamofire.request(movieSearchURL, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil)
    }
}
