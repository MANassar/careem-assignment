//
//  MovieListViewController.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 06/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import UIKit
import Crashlytics

class SearchViewController: UITableViewController
{
    //
    // MARK: - Constants
    //
    let moviesPresenterStoryboardID = "moviesTableViewPresenter"
    let searchCellIdentifier = "searchCell"
    
    //
    // MARK:- Strings
    //
    let noResultsErrorMessage = "No results found. Please try a different search."
    let unknownErrorMessage = "We have encountered an unknown error"
    let searchHistorySectionTitle = "Search History"
    
    //
    // MARK: - Variables
    //
    var searchController:UISearchController?
    var moviesTablePresenter:MoviesTableViewPresenter?
    {
        didSet
        {
            moviesTablePresenter?.delegate = self
        }
    }
    let searchHistoryFilterTableViewController = UITableViewController(style: .plain)
    
    //
    //MARK: Search variables
    //
    var moviesArray:[Movie]?
    var currentPage:Int?
    var totalPages:Int?
    var currentSearchText:String?
    var searchHistory:[String]?
    var filteredSearchHistory:[String]?
    
    //
    // MARK:- View lifecycle
    //
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Init search controller stuff
        searchHistoryFilterTableViewController.tableView.dataSource = self
        searchHistoryFilterTableViewController.tableView.delegate = self
        searchHistoryFilterTableViewController.view.backgroundColor = UIColor.black
        searchHistoryFilterTableViewController.tableView.backgroundColor = UIColor.black
        
        self.searchController = UISearchController(searchResultsController: searchHistoryFilterTableViewController)
        self.searchController?.searchResultsUpdater = self
        self.tableView.tableHeaderView = self.searchController?.searchBar
        self.searchController?.searchBar.barStyle = .black
        self.searchController?.searchBar.delegate = self
        self.definesPresentationContext = true
        
        //Try to get the movies controller
        if let storyboard = self.storyboard, let moviesPresenter = storyboard.instantiateViewController(withIdentifier: moviesPresenterStoryboardID) as? MoviesTableViewPresenter
        {
            self.moviesTablePresenter = moviesPresenter
        }
        
        self.searchHistory = PersistentStorage.getSearchHistory()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.reloadTableViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //
    // MARK:- Helper/Convienience functions
    //
    func gotSearchHistory() -> Bool
    {
        if self.searchHistory != nil, self.searchHistory!.count > 0
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func gotFilteredResults() -> Bool
    {
        if self.filteredSearchHistory != nil, self.filteredSearchHistory!.count > 0
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func reloadTableViews()
    {
        self.tableView.reloadData()
        self.searchHistoryFilterTableViewController.tableView.reloadData()
    }
    
    //
    // MARK:- Tableview datasource and delegate
    //
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == self.tableView
        {
            return self.gotSearchHistory() ? 1 : 0
        }
        else
        {
            return self.gotFilteredResults() ? 1 : 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == self.tableView
        {
            return self.gotSearchHistory() ? self.searchHistory!.count : 0
        }
        else
        {
            return self.gotFilteredResults() ? self.filteredSearchHistory!.count : 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if tableView == self.tableView
        {
            return self.gotSearchHistory() ? searchHistorySectionTitle : ""
        }
        else
        {
            return self.gotFilteredResults() ? searchHistorySectionTitle : ""
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: searchCellIdentifier)
        if cell == nil
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: searchCellIdentifier)
        }
        
        cell?.backgroundColor = UIColor.black
        
        cell!.textLabel?.textColor = UIColor.white
        cell!.textLabel?.text = searchHistory?[indexPath.row]
        
        if tableView == self.tableView
        {
            cell!.textLabel?.text = searchHistory?[indexPath.row]
        }
        else //Filter table
        {
            cell!.textLabel?.text = filteredSearchHistory?[indexPath.row]
        }
        
        return cell!
    }
    
    //
    // MARK:- Tableview delegates
    //
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var searchText:String
        if tableView == self.tableView
        {
            searchText = searchHistory![indexPath.row]
        }
        else
        {
            searchText = filteredSearchHistory![indexPath.row]
        }
        
        self.searchController?.searchBar.text = searchText
        self.performSearch(searchString: searchText)
    }
    
    func performSearch(searchString:String)
    {
        self.currentSearchText = searchString
        
        API_Interface.getMovies(searchQuery: searchString) { (movieResponse, error) in
            self.handleSearchReponse(movieResponse, error: error)
        }
        
        Answers.logSearch(withQuery: searchString, customAttributes: nil)
    }
}

//
// MARK:- Extensions for Delegate implementations
//

extension SearchViewController:UISearchResultsUpdating, UISearchControllerDelegate
{
    func updateSearchResults(for searchController: UISearchController)
    {
        if let searchString = self.searchController?.searchBar.text, self.gotSearchHistory()
        {
            let filterResults = self.searchHistory?.filter({ (searchHistoryItem) -> Bool in
                return searchHistoryItem.lowercased().starts(with: searchString.lowercased())
            })
            
            self.filteredSearchHistory = nil
            
            if filterResults != nil, filterResults!.count > 0
            {
                self.filteredSearchHistory = filterResults
            }
            
            self.searchHistoryFilterTableViewController.tableView.reloadData()
        }
    }
}

//
// MARK:- Extension for Movie Presenter delegate
//
extension SearchViewController: MoviesTableViewPresenterDelegate
{
    func getMoreData()
    {
        if !isLastPage(), currentPage != nil, currentSearchText != nil
        {
            currentPage! += 1
            API_Interface.getMovies(searchQuery: currentSearchText!, page: currentPage!) { (movieResponse, error) in
                self.handleSearchReponse(movieResponse, error: error)
            }
        }
    }
    
    func isLastPage() -> Bool
    {
        if currentPage != nil, totalPages != nil, currentPage! < totalPages!
        {
            return false
        }
        else
        {
            return true
        }
    }
}

//
// MARK:- Extension for Search bar delegate
//

extension SearchViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText == ""
        {
            moviesTablePresenter?.resetView()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if let searchText = searchBar.text
        {
            self.performSearch(searchString: searchText)
        }
    }
    
    fileprivate func handleSearchReponse(_ movieResponse: MovieResponse?, error:Error?)
    {
        if movieResponse != nil
        {
            self.updateSearchResultsWithResponse(movieResponse: movieResponse!)
            if moviesTablePresenter != nil, self.navigationController?.topViewController != moviesTablePresenter!
            {
                self.navigationController?.pushViewController(moviesTablePresenter!, animated: true)
            }
        }
        else if error != nil
        {
            self.displayErrorAlert(error!, message: nil)
        }
    }
    
    func updateSearchHistory(_ newString:String)
    {
        if PersistentStorage.updateSearchHistory(newString)
        {
            self.searchHistory = PersistentStorage.getSearchHistory()
            self.reloadTableViews()
        }
    }
    
    func updateSearchResultsWithResponse(movieResponse:MovieResponse)
    {
        if let moviesArray = movieResponse.results, moviesArray.count > 0
        {
            if currentPage != nil, currentPage! > 1, movieResponse.page! == currentPage! //Continuation of an old search, update the result
            {
                self.moviesTablePresenter?.addMovies(additionalMovies: moviesArray)
            }
            else //New search
            {
                self.moviesTablePresenter?.updateMovieArray(newMovieArray: moviesArray)
                self.updateSearchHistory(currentSearchText!)
            }
        }
        else
        {
            self.displayErrorAlert(nil, message: noResultsErrorMessage)
        }
        
        if let currentPage = movieResponse.page
        {
            self.currentPage = currentPage
        }
        
        if let totalPages = movieResponse.total_pages
        {
            self.totalPages = totalPages
        }
    }
    
    func displayErrorAlert(_ error:Error?, message:String?)
    {
        var errorMessage:String!
        if error != nil
        {
            errorMessage = error?.localizedDescription
        }
        else if message != nil
        {
            errorMessage = message
        }
        else
        {
            errorMessage = unknownErrorMessage
        }
        
        let alertController = UIAlertController(title: "Sorry", message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
