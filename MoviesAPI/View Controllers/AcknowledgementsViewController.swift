//
//  AcknowledgementsViewController.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 12/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import UIKit

class AcknowledgementsViewController: UIViewController {
    
    //
    // MARK:- Variables
    //
    @IBOutlet weak var acknowledgementsTextView: UITextView!
    var acknowledgementsDictionary:[String:Any]?
    
    let acknowledgementLicenseKey = "FooterText"
    let acknowledgementTitleKey = "Title"
    let mainArrayKey = "PreferenceSpecifiers"
    let titleString = "This application makes use of the following third party libraries:"
    
    //
    // MARK:- Functions
    //
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if let acknowledgementsPlistPath = Bundle.main.path(forResource: "Acknowledgements", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: acknowledgementsPlistPath) as? [String:Any]
        {
            if let mainArray = dictionary[mainArrayKey] as? [[String:Any]]
            {
                acknowledgementsDictionary = mainArray[1]
                if let acknowledgementText = acknowledgementsDictionary?[acknowledgementLicenseKey] as? String, let libraryTitle = acknowledgementsDictionary?[acknowledgementTitleKey] as? String
                {
                    self.acknowledgementsTextView.text = titleString + "\n\n" + libraryTitle + "\n\n" + acknowledgementText
                    self.acknowledgementsTextView.scrollRangeToVisible(NSMakeRange(0, 1))
                }
            }
            
//            self.acknowledgementsTextView.text = acknowledgementsDictionary
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
