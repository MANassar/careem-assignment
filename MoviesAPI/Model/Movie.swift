//
//  Movie.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 06/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import Foundation

struct Movie: Decodable
{
    //
    // MARK:- Variables
    //
    var vote_count:Int?
    var vote_average:Float?
    var id:Int?
    var video:Bool?
    var title:String?
    var popularity:Float?
    var poster_path:String?
    var original_language:String?
    var original_title:String?
    var genre_ids:[Int]?
    var backdrop_path:String?
    var adult:Bool?
    var overview:String?
    var release_date:String?
}
