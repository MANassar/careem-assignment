//
//  MovieResponse.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 07/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import Foundation

struct ResponseKeys
{
    static let page = "page"
    static let total_results = "total_results"
    static let total_pages = "total_pages"
    static let results = "results"
}

struct MovieResponse:Decodable
{
    let page:Int?
    let total_results:Int?
    let total_pages:Int?
    var results:[Movie]?
}
