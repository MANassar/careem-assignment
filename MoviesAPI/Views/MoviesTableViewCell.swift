//
//  MoviesTableViewCell.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 08/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell
{
    //
    // MARK:- Constants
    //
    static let cellIdentifier = "moviesCell"
    
    //
    // MARK:- IBOutlets
    //
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieReleaseDateLabel: UILabel!
    @IBOutlet weak var movieOverviewLabel: UILabel!
    
    //
    // MARK:- Variables
    //
    var movie:Movie?
    {
        didSet
        {
            //Clear the previous image
            self.posterImageView.image = #imageLiteral(resourceName: "ImagePlaceholder")
            
            if let movieTitle = movie?.title
            {
                self.movieTitleLabel.text = movieTitle
            }
            
            if let movieReleaseDate = movie?.release_date
            {
                self.movieReleaseDateLabel.text = movieReleaseDate
            }
            
            if let movieOverview = movie?.overview
            {
                self.movieOverviewLabel.text = movieOverview
            }
            
            if let moviePosterURL = movie?.poster_path
            {
                API_Interface.getImage(urlPostfix: moviePosterURL, width: ImageWidth.Width_92) { (posterImage, error) in
                    if posterImage != nil
                    {
                        self.posterImageView.image = posterImage
                    }
                }
            }
        }
    }
    
    //
    // MARK:- Functions
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
