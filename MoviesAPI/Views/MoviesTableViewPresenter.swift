//
//  MoviesTableViewPresenter.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 07/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import UIKit
import Dynatrace

class MoviesTableViewPresenter: UITableViewController
{
    //
    // MARK:- Constants
    //
    let defaultCellHeight = 150
    
    let lastCellText = "That's it, Folks!"
    let loadingCellText = "Loading next page..."
    let loadingMoreCellIdentifier = "loadingCell"
    let lastCellIdentifier = "lastCell"
    let backupCellIdentifier = "backupCell"
    
    //
    // MARK:- Variables
    //
    private var moviesArray: [Movie]?
    var delegate: MoviesTableViewPresenterDelegate?
    
    //
    // MARK:- View lifecycle
    //
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //
    // MARK:- Data Retrieval methods
    //
    func addMovies(additionalMovies:[Movie])
    {
        if self.moviesArray == nil
        {
            self.moviesArray = [Movie]()
        }
        
        self.moviesArray!.append(contentsOf: additionalMovies)
        self.tableView.reloadData()
    }
    
    /*
     This function is to replace the whole movies array
     */
    func updateMovieArray(newMovieArray:[Movie])
    {
        self.moviesArray = newMovieArray
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    func resetView()
    {
        if moviesArray != nil //So that we dont double reset
        {
            self.moviesArray = nil
            let sectionIndexSet = IndexSet.init(integer: 0)
            self.tableView.deleteSections(sectionIndexSet, with: .automatic)
        }
    }
    
    //
    // MARK: - Table view data source
    //
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return (moviesArray != nil) ? 1 : 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (moviesArray?.count != nil) ? moviesArray!.count + 1 : 0 //The +1 is to dispaly a loading cell
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == moviesArray!.count //This is the last cell, ask the delegate if we still have more cells
        {
            var backupCell:UITableViewCell?
            
            if let isLastPage = delegate?.isLastPage(), isLastPage == false //We still have more data to retrieve
            {
                if let loadingCell = tableView.dequeueReusableCell(withIdentifier: loadingMoreCellIdentifier)
                {
                    return loadingCell
                }
                else
                {
                    backupCell = UITableViewCell(style: .default, reuseIdentifier: loadingMoreCellIdentifier)
                    backupCell?.textLabel?.text = loadingCellText
                    return backupCell!
                }
            }
            else
            {
                if let lastCell = tableView.dequeueReusableCell(withIdentifier: lastCellIdentifier)
                {
                    return lastCell
                }
                else
                {
                    backupCell = UITableViewCell(style: .default, reuseIdentifier: lastCellIdentifier)
                    backupCell?.textLabel?.text = lastCellText
                    return backupCell!
                }
            }
        }
        
        if let moviesCell = tableView.dequeueReusableCell(withIdentifier: MoviesTableViewCell.cellIdentifier, for: indexPath) as? MoviesTableViewCell
        {
            moviesCell.movie = moviesArray![indexPath.row]
            return moviesCell
        }
        
        else
        {
            return tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath)
        }
    }
    
    //
    // MARK:- Tableview delegate
    //
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //Nothing really to do, just be graceful
        tableView.deselectRow(at: indexPath, animated: true)
        
//        let action = DTXAction.enter(withName: "Starting error creation action")
        
        //Dynatrace test - Report error to appmon
        var errorDescription = ""
        if let cell = self.tableView(tableView, cellForRowAt: indexPath) as? MoviesTableViewCell
        {
            errorDescription = cell.movieTitleLabel.text!
        }
        else
        {
            errorDescription = "Error manually reported"
        }
        let error = NSError(domain: "NassarErrorDomain", code: 666, userInfo: [NSLocalizedDescriptionKey:errorDescription, NSLocalizedFailureReasonErrorKey: "Because I said so"])

        DTXAction.reportError(withName: "Nassar Error", error: error)
        
//        action!.reportError(withName: "Nassar Error", error: error)
//        action!.leave()
        
//        let fakeIndexPath = IndexPath(row: 300, section: 2)
//        tableView.selectRow(at: fakeIndexPath, animated: true, scrollPosition: .middle)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if let count = moviesArray?.count, indexPath.row == count, let _ = delegate?.isLastPage() //Last cell. Try to get more pages from the delegate
        {
            delegate?.getMoreData()
        }
    }
}

protocol MoviesTableViewPresenterDelegate
{
    func getMoreData()
    func isLastPage() -> Bool
}
