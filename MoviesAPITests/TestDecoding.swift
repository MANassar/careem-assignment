//
//  MoviesAPITests.swift
//  MoviesAPITests
//
//  Created by Mohamed Nassar on 06/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import XCTest
@testable import MoviesAPI

class TestDecoding: XCTestCase
{
    //
    // MARK:- Variables
    //
    var mockMovieResponseJSON = """
{
  "page": 1,
  "total_results": 3,
  "total_pages": 2,
  "results": [
    {
      "vote_count": 2873,
      "id": 268,
      "video": false,
      "vote_average": 7.1,
      "title": "Batman",
      "popularity": 16.444495,
      "poster_path": "/kBf3g9crrADGMc2AMAMlLBgSm2h.jpg",
      "original_language": "en",
      "original_title": "Batman",
      "genre_ids": [
        14,
        28
      ],
      "backdrop_path": "/2blmxp2pr4BhwQr74AdCfwgfMOb.jpg",
      "adult": false,
      "overview": "The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.",
      "release_date": "1989-06-23"
    },
    {
      "vote_count": 2304,
      "id": 364,
      "video": false,
      "vote_average": 6.7,
      "title": "Batman Returns",
      "popularity": 14.760571,
      "poster_path": "/ifzddUhnsTf1h6guBUKBlDwuS1t.jpg",
      "original_language": "en",
      "original_title": "Batman Returns",
      "genre_ids": [
        28,
        14
      ],
      "backdrop_path": "/reQewz6KndV7Js2mfLfaaX8Rj0T.jpg",
      "adult": false,
      "overview": "Having defeated the Joker, Batman now faces the Penguin - a warped and deformed individual who is intent on being accepted into Gotham society. Crooked businessman Max Schreck is coerced into helping him become Mayor of Gotham and they both attempt to expose Batman in a different light. Selina Kyle, Max's secretary, is thrown from the top of a building and is transformed into Catwoman - a mysterious figure who has the same personality disorder as Batman. Batman must attempt to clear his name, all the time deciding just what must be done with the Catwoman.",
      "release_date": "1992-06-19"
    }
  ]
}
"""
    
    //
    // MARK:- Setup funcs
    //
    override func setUp()
    {
        super.setUp()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    //
    // MARK:- Test funcs
    //
    func testMovieResponseParsing()
    {
        guard let jsonData = mockMovieResponseJSON.data(using: .utf8) else//JSONSerialization.data(withJSONObject: mockMovieResponseJSON, options: .prettyPrinted)
        {
            XCTFail("Failed to serialize JSON string into data")
            return
        }
        XCTAssertNotNil(jsonData, "Failed to serialize JSON string into data")
        
        guard let decodedMovieResponse:MovieResponse = ResponseDecoder.decodeResponseJSONData(jsonData) else
        {
            XCTFail("Failed to decode movie response")
            return 
        }
        
        let expectedTotalPages = 2
        let expectedTotalResults = 3
        let expectedPage = 1
        let expectedResultCount = 2
        
        XCTAssertEqual(expectedPage, decodedMovieResponse.page)
        XCTAssertEqual(expectedTotalPages, decodedMovieResponse.total_pages)
        XCTAssertEqual(expectedTotalResults, decodedMovieResponse.total_results)
        XCTAssertEqual(expectedResultCount, decodedMovieResponse.results?.count)
    }
    
    
    
}
