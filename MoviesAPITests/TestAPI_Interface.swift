//
//  TestAPI_Interface.swift
//  MoviesAPITests
//
//  Created by Mohamed Nassar on 12/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import XCTest
@testable import MoviesAPI

class TestAPI_Interface: XCTestCase
{
    //
    // MARK:- Variables
    //
    var movieSearchString:String!
    var imageURLPostfix:String!
    var imageWidth:ImageWidth!
    var page:Int!
    
    //
    // MARK:- Setup funcs
    //
    
    override func setUp()
    {
        super.setUp()
        movieSearchString = "batman"
        imageURLPostfix = "/2DtPSyODKWXluIRV7PVru0SSzja.jpg"
        imageWidth = ImageWidth.Width_185
        page = 1
    }
    
    override func tearDown()
    {
        movieSearchString = nil
        imageURLPostfix = nil
        super.tearDown()
    }
    
    //
    // MARK:- Test funcs
    //
    
    func testMovieDataRequest()
    {
        let dataRequest = API_Interface.prepareMoviesDataRequest(movieSearchString)
        XCTAssertNotNil(dataRequest)
        
        let movieURL = dataRequest!.request?.url?.absoluteString
        XCTAssertNotNil(movieURL)
        
        let expectedURLString = "http://api.themoviedb.org/3/search/movie?api_key=2696829a81b1b5827d515ff121700838&page=1&query=batman"
        XCTAssertEqual(movieURL, expectedURLString)
    }
    
//    func testGetImageURLForMovie()
//    {
//        let expectedImageURL = "​http://image.tmdb.org/t/p/w185/2DtPSyODKWXluIRV7PVru0SSzja.jpg​"
//        let imageURL = API_Interface.getImageURL(imageURLPostfix, imageWidth)
//        XCTAssertNotNil(imageURL)
//        
//        let imageURLString = imageURL!.absoluteString
//        
//        print(imageURLString)
//        print(expectedImageURL)
//        XCTAssertEqual(imageURLString, expectedImageURL)
//    }
    
}
