//
//  TestPersistentStorage.swift
//  MoviesAPITests
//
//  Created by Mohamed Nassar on 12/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import XCTest
@testable import MoviesAPI

class TestPersistentStorage: XCTestCase
{
    //
    // MARK:- Variables
    //
    
    var searchHistoryItems:[String]!
    
    //
    // MARK:- Setup
    //
    override func setUp()
    {
        super.setUp()
        
        searchHistoryItems = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
        PersistentStorage.clearSearchHistory() //Make sure we start clean
    }
    
    override func tearDown()
    {
        searchHistoryItems = nil
        PersistentStorage.clearSearchHistory()
        
        super.tearDown()
    }
    
    //
    // MARK:- Test methods
    //
    func testSavingSearchHistory()
    {
        //Confirm we have nothing first
        XCTAssertNil(PersistentStorage.getSearchHistory())
        
        //Add array
        PersistentStorage.saveSearchHistory(searchHistory: searchHistoryItems)
        
        let searchHistory = PersistentStorage.getSearchHistory()
        XCTAssertNotNil(searchHistory)
        XCTAssertEqual(searchHistory!.count, searchHistoryItems.count)
    }
    
    func testUpdatingHistory()
    {
        PersistentStorage.saveSearchHistory(searchHistory: searchHistoryItems)
        
        let item10 = "10"
        let item11 = "11'"
        
        let expectedSuccess = PersistentStorage.updateSearchHistory(item10) //This is the 10th item
        XCTAssertTrue(expectedSuccess) //We should be able to add it successfully
        
        var searchHistory = PersistentStorage.getSearchHistory()
        XCTAssertNotNil(searchHistory)
        
        XCTAssertEqual(searchHistory![0], item10)
        
        let expectedFailure = PersistentStorage.updateSearchHistory(item10) //we shouldnt be able to add the same item twice
        XCTAssertFalse(expectedFailure)
        
        let lastItem = searchHistory!.last
        
        let replacementSuccess = PersistentStorage.updateSearchHistory(item11) //This will push out the last item as in queue
        XCTAssertTrue(replacementSuccess)
        
        searchHistory = PersistentStorage.getSearchHistory()
        XCTAssertNotNil(searchHistory)
        
        XCTAssertEqual(searchHistory![0], item11)
        XCTAssertFalse(searchHistory!.contains(lastItem!))
    }
    
}
